import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class AmazonTeste {

    private WebDriver driver;

    @Before
    public void abrir() {
        System.setProperty("webdriver.gecko.driver","C:/driver/geckodriver.exe");
        //instanciando o objeto driver
        driver = new FirefoxDriver();
        //tela cheia
        driver.manage().window().maximize();
        //link do site que quer abrir
        driver.get("https://www.amazon.com.br/");
    }

    @After
    public void sair(){
        driver.quit();
    }

    @Test
    public void maisVendidos() throws InterruptedException {
        //faz o programa esperar
        Thread.sleep(3000);
        //encontra o lugar a ser clicado pelo id e clica
        driver.findElement(By.id("nav-hamburger-menu")).click();
        Thread.sleep(3000);
        //encontra o elemento pelo xpath e clica também
        driver.findElement(By.xpath("/html/body/div[3]/div[2]/div/ul[1]/li[2]/a")).click();
        Thread.sleep(3000);
        //compara o texto para ver ser o mais vendidos realmente abriu atraves do Assert.assertEquals();
        Assert.assertEquals("Mais vendidos", driver.findElement(By.xpath("//*[@id=\"zg_banner_text\"]")).getText());
        Thread.sleep(3000);
    }

    @Test
    public void testarBuscador() throws InterruptedException {
        driver.findElement(By.id("twotabsearchtextbox")).sendKeys("Qualquer");
        driver.findElement(By.id("nav-search-submit-button")).click();
        Thread.sleep(3000);
        Assert.assertEquals("RESULTADOS", driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[1]/div[1]/div/span[3]/div[2]/div[1]/div/span/div/div/span")).getText());
        Thread.sleep(3000);
    }

    @Test
    public void testarAutenticacaoSenhaIncorreta() throws InterruptedException {
        driver.findElement(By.id("nav-link-accountList")).click();
        driver.findElement(By.id("ap_email")).sendKeys("aasojioasi@gmail.com");
        driver.findElement(By.id("continue")).click();
        Assert.assertEquals("Não encontramos uma conta associada a este endereço de e-mail",driver.findElement(By.xpath("/html/body/div[1]/div[1]/div[2]/div/div[1]/div/div/div/div/ul/li/span")).getText());
        Thread.sleep(3000);
    }

    @Test
    public void testarBuscarLivro() throws InterruptedException {
        Thread.sleep(7000);
        driver.findElement(By.xpath("/html/body/div[1]/header/div/div[4]/div[2]/div[2]/div/a[6]")).click();
        Thread.sleep(3000);
        driver.findElement(By.id("twotabsearchtextbox")).sendKeys("Java");
        Thread.sleep(3000);
        driver.findElement(By.id("nav-search-submit-button")).click();
        Thread.sleep(5000);
        Assert.assertEquals("RESULTADOS", driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[1]/div[1]/div/span[3]/div[2]/div[1]/div/span/div/div/span")).getText());
        Thread.sleep(5000);
    }

    @Test
    public void testarNovidadesAmazon() throws InterruptedException {
        Thread.sleep(5000);
        driver.findElement(By.id("nav-hamburger-menu")).click();
        Thread.sleep(5000);
        driver.findElement(By.xpath("/html/body/div[3]/div[2]/div/ul[1]/li[3]/a")).click();
        Thread.sleep(5000);
        Assert.assertEquals("Novidades na Amazon", driver.findElement(By.id("zg_banner_text")).getText());
    }

    @Test
    public void testeBuscarMouseGamer(){
        driver.findElement(By.id("twotabsearchtextbox")).sendKeys("Mouse gamer");
        driver.findElement(By.id("nav-search-submit-button")).click();
        Assert.assertEquals("Jogue como os profissionais.", driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[1]/div[1]/div/span[3]/div[2]/div[1]/div/div/div[1]/div[1]/div/div[2]/div/a/span/span[2]")).getText());
    }

    @Test
    public void testeAbrirPrimeVideo() throws InterruptedException {
        driver.findElement(By.id("nav-hamburger-menu")).click();
        Thread.sleep(7000);
        driver.findElement(By.xpath("/html/body/div[3]/div[2]/div/ul[1]/li[9]/a")).click();
        Thread.sleep(7000);
        driver.findElement(By.xpath("/html/body/div[3]/div[2]/div/ul[4]/li[3]/a")).click();
        Thread.sleep(7000);
        Assert.assertEquals("Watch movies and TV shows", driver.findElement(By.className("dv-copy-title")).getText());
    }

}
